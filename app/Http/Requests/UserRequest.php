<?php

namespace App\Http\Requests;

use App\Models\User;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($requestedUser = $this->route('user')) {
            return $requestedUser->id === auth()->user()->id;
        } else {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return User::getCreateRules();
            case 'PATCH':
            case 'PUT':
                return User::getUpdateRules($this->route('user'));
            default:
                return [];
        }
    }
}
