<?php

namespace App\Models;

use App\Traits\Validatable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Validatable;

    // TODO: Move validation rules to request
    protected static $createRules = [
        'name'     => 'required|string|min:2|max:16',
        'email'    => 'required|email|unique:users',
        'password' => 'required|string|min:8',
    ];

    protected static $updateRules = [
        'name'     => 'sometimes|required|string|min:2|max:16',
        'email'    => 'sometimes|required|email|unique:users,email,%id%,id',
        'password' => 'sometimes|required|string|min:8',
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = ['password'];
    protected $dates = ['deleted_at'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
